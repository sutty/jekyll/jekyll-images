# frozen_string_literal: true

module Jekyll
  module Filters
    # Liquid filter for use in templates
    module Thumbnail
      # Generates a thumbnail and returns its alternate destination
      def thumbnail(input, width = nil, height = nil, crop = nil, auto_rotate = nil)
        return input unless input
        return input if input.downcase.end_with? '.gif'
        return input unless Jekyll.env == 'production'

        path = @context.registers[:site].in_source_dir input

        unless ::File.exist? path
          Jekyll.logger.warn "File doesn't exist #{input}"
          return input
        end

        image = Jekyll::Images::Cache.cached_image path
        thumb = image.thumbnail(width: width,
                                height: height,
                                crop: crop,
                                auto_rotate: auto_rotate)

        thumb.write && thumb.optimize

        thumb.url
      rescue Vips::Error => e
        Jekyll.logger.warn "Failed to process #{input}: #{e.message}"
        input
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Thumbnail)
