# frozen_string_literal: true

module Jekyll
  module Filters
    # Liquid filter for use in templates
    module Dzsave
      # Generates an image pyramid
      #
      # @see {https://www.libvips.org/API/current/Making-image-pyramids.md.html}
      def dzsave(input, layout = 'google', tile_size = 256)
        return input unless input

        site = @context.registers[:site]
        path = site.in_source_dir(input)

        unless ::File.exist?(path)
          Jekyll.logger.warn "File doesn't exist #{input}"
          return input
        end

        dir = ::File.dirname(input)
        previous_tiles = []

        Dir.chdir(site.source) do
          previous_tiles = Dir.glob(::File.join(dir, '*', '**', '*.jpg'))
        end

        image = Vips::Image.new_from_file(path, access: :sequential)
        image.dzsave(dir, layout: layout, tile_size: tile_size)

        Dir.chdir(site.source) do
          (Dir.glob(::File.join(dir, '*', '**', '*.jpg')) - previous_tiles).each do |tile|
            site.static_files << Jekyll::StaticFile.new(site, site.source, ::File.dirname(tile), ::File.basename(tile))
          end
        end

        "#{dir}/{z}/{y}/{x}.jpg"
      rescue Vips::Error => e
        Jekyll.logger.warn "Failed to process #{input}: #{e.message}"
        input
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Dzsave)
