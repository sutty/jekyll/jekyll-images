# frozen_string_literal: true

module Jekyll
  module Filters
    # Maximum zoom level
    module MaxZoomLevel
      # Finds the maximum zoom level for an image
      #
      # @param :input [String]
      # @return [Integer]
      def max_zoom_level(input)
        return input unless input

        path = ::File.dirname(@context.registers[:site].in_source_dir(input))

        unless ::File.exist? path
          Jekyll.logger.warn 'Max zoom level:', "File doesn't exist #{input}"
          return input
        end

        max_zoom_level = 0

        loop do
          unless ::File.directory?(::File.join(path, max_zoom_level.to_s))
            max_zoom_level -= 1
            break
          end

          break if max_zoom_level > 18

          max_zoom_level += 1
        end

        max_zoom_level
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::MaxZoomLevel)
