# frozen_string_literal: true

module Jekyll
  module Filters
    # Obtain image height
    module Height
      def height(input)
        return unless input
        path = @context.registers[:site].in_source_dir input

        unless ::File.exist? path
          Jekyll.logger.warn "File doesn't exist #{input}"
          return input
        end

        Jekyll::Images::Cache.cached_image(path).height
      rescue Vips::Error => e
        Jekyll.logger.warn "Failed to process #{input}: #{e.message}"
        nil
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Height)
