# frozen_string_literal: true

module Jekyll
  module Images
    # Cache
    class Cache
      class << self
        attr_accessor :site

        def cached_images
          @cached_images ||= {}
        end

        def cached_image?(input)
          cached_images.key? input
        end

        def cached_image(input)
          cached_images[input] ||= Jekyll::Images::Image.new(site, input)
        end
      end
    end
  end
end
