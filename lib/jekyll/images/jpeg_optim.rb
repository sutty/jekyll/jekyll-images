# frozen_string_literal: true

require_relative 'runner'

module Jekyll
  module Images
    # Runs jpegoptim on JPEG files
    class JpegOptim < Runner
      BINARY = 'jpegoptim'.freeze

      def command
        [binary, '--strip-all', '--quiet', ('--all-progressive' if interlaced), file].compact
      end
    end
  end
end

Jekyll::Images::Runner.register('jpeg', Jekyll::Images::JpegOptim)
