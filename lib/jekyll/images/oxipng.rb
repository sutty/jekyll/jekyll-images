# frozen_string_literal: true

require_relative 'runner'

module Jekyll
  module Images
    # Runs oxipng on PNG files
    class Oxipng < Runner
      BINARY = 'oxipng'.freeze

      def command
        [binary, '--strip', 'all', '--quiet', '--interlace', (interlaced ? %w[1 --force] : '0'), file].flatten
      end
    end
  end
end

Jekyll::Images::Runner.register('png', Jekyll::Images::Oxipng)
