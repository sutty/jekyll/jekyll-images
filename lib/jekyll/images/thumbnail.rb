# frozen_string_literal: true

require 'vips'

module Jekyll
  module Images
    class Thumbnail
      attr_reader :site, :filename, :image, :width, :height, :crop, :auto_rotate

      def initialize(site:, filename:, image:, **args)
        if args.slice(:width, :height).values.none?
          raise ArgumentError, "#{filename} thumbnail needs width or height"
        end

        @site = site
        @filename = filename
        @image = image
        @width = args[:width] || proportional_width(args[:height])
        @height = args[:height] || proportional_height(args[:width])
        @crop = args[:crop]&.to_sym || :attention
        @auto_rotate = args[:auto_rotate].nil? ? true : args[:auto_rotate]
      end

      # XXX: We don't memoize because we don't need the thumbnail in
      # memory after it has been written.
      def thumbnail
        Vips::Image.thumbnail filename,
                              width,
                              height: height,
                              auto_rotate: auto_rotate,
                              crop: crop
      end

      # Finds a height that's proportional to the width
      def proportional_height(width)
        @proportional_height ||= (image.height * (width / image.width.to_f)).round
      end

      # Find a width that's proportional to height
      def proportional_width(height)
        @proportional_width ||= (image.width * (height / image.height.to_f)).round
      end

      # Generates a destination from filename only if we're downsizing
      def dest
        @dest ||= if thumbnail?
                    filename.gsub(/#{extname}\z/, "_#{width}x#{height}#{extname}")
                  else
                    filename.gsub(/#{extname}\z/, "_optimized#{extname}")
                  end
      end

      def url
        static_file.url
      end

      def thumbnail?
        image.width > width
      end

      def extname
        @extname ||= File.extname filename
      end

      # Only write when the source is newer than the thumbnail
      def write?
        !File.exist?(dest) || File.mtime(filename) > File.mtime(dest)
      end

      # Save the file into destination if needed and add to files to
      # copy
      def write
        return unless write?

        if thumbnail?
          Jekyll.logger.info "Thumbnailing #{filename} => #{dest}"
          thumbnail.write_to_file(dest)
        else
          Jekyll.logger.info "Copying #{filename} => #{dest}"
          FileUtils.cp(filename, dest)
        end

        # Add it to the static files so Jekyll copies them.  Once they
        # are written they're copied when the site is loaded.
        site.static_files << static_file

        # The file was updated, so it exists and is newer than source
        !write?
      end

      # Run optimizations
      def optimize
        before, after = Runner.run(dest, interlaced?)

        return unless before

        pct = ((after.to_f / before) * -100 + 100).round(2)

        Jekyll.logger.info "Reduced #{dest} from #{before} to #{after} bytes (%#{pct})"
      end

      def relative_path
        @relative_path ||= dest.sub(site.source, '').sub(%r{\A/}, '')
      end

      def static_file
        @static_file ||= Jekyll::StaticFile.new(site, site.source, File.dirname(relative_path), File.basename(relative_path))
      end

      def interlaced?
        site.config.dig('images', 'interlaced')
      end
    end
  end
end
