# frozen_string_literal: true

require 'vips'

module Jekyll
  module Images
    class Image
      attr_reader :site, :filename, :height, :width

      def initialize(site, filename)
        unless File.exist? filename
          raise ArgumentError, "File not found: #{filename}"
        end

        @cached_thumbnails = {}
        @site = site
        @filename = filename

        # We only need the image to get height and width
        image = Vips::Image.new_from_file filename, access: :sequential

        @height = image.height
        @width = image.width
      end

      def thumbnail(**args)
        @cached_thumbnails[args.hash.to_s] ||= Thumbnail.new site: site, filename: filename, image: self, **args
      end
    end
  end
end
