# frozen_string_literal: true

require 'open3'
require 'filemagic'

module Jekyll
  module Images
    # Runner for optimizations
    class Runner
      attr_reader :binary, :bytes_after, :bytes_before, :file, :interlaced

      class << self
        # XXX: This only allows one optimization per file type, we could
        # make it an array but how do we garantee order?  Maybe adding a
        # priority field?
        def register(mime, class_name)
          @@runners ||= {}
          @@runners[mime] = class_name
        end

        def runners
          @@runners
        end

        def mime(file)
          @@mime ||= FileMagic.new

          @@mime.file(file, true)
        end

        def run(file, interlaced = false)
          type = mime(file)
          runners[type].new(file, interlaced).run if runners[type]
        end
      end

      def initialize(file, interlaced = false)
        @file = file
        @bytes_before = bytes_after
        @interlaced = interlaced
      end

      def bytes_after
        File.size file
      end

      def exist?
        @binary, _, status = Open3.capture3("which #{self.class::BINARY}")
        @binary.chomp!

        status.success?
      end

      def run
        return unless exist?

        _, status = Open3.capture2e(*command)

        [bytes_before, bytes_after] if status.success?
      end
    end
  end
end
