# frozen_string_literal: true

require_relative 'jekyll/images/cache'

Jekyll::Hooks.register :site, :after_reset do |site|
  Jekyll::Images::Cache.site = site
end

require_relative 'jekyll/images/image'
require_relative 'jekyll/images/thumbnail'
require_relative 'jekyll/filters/thumbnail'
require_relative 'jekyll/filters/width'
require_relative 'jekyll/filters/height'
require_relative 'jekyll/filters/dzsave'
require_relative 'jekyll/filters/max_zoom_level'
require_relative 'jekyll/images/oxipng'
require_relative 'jekyll/images/jpeg_optim'
