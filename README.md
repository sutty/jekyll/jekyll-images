# Image optimization for Jekyll

This Jekyll plugin helps you optimize images by thumbnailing them to
the sizes you specify in templates.

It uses [Libvips](https://libvips.github.io/libvips/) for performance
and low resource usage.  It'll also cache the thumbnails so it only runs
once, but it'll update the thumbnail if the source image changes (or is
`touch`ed).

The thumbnails are smartly cropped by default to point of attention, so
they're more useful than just cropping at the center, but you can change
this (see below).

It will run `jpegoptim` and `oxipng` if it can find the binaries
installed for extra optimization.

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'jekyll-images'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-images

For image optimization, install packages containing `jpegoptim` and
`oxipng`.

## Configuration

Everything is handled automatically.  If image optimizers are found,
thumbnails are automatically optimized.

### Interlaced / Progressive images

Interlaced images makes them appear to load faster, because the image
loads dimensions and gains quality as it downloads.   According to
Cloudflare, at 50% download the image looks almost like the final
image[^0], even though interlaced images tend to use more disk space.

To enable interlacing, add to `_config.yml`:

```yaml
images:
  interlaced: true
```

**Note:** Interlacing sometimes won't make the images smaller.

[^0]:
  <https://blog.cloudflare.com/parallel-streaming-of-progressive-images/>

## Usage

### Only builds on production

To perform actual thumbnailing, run `jekyll build` in production mode:

```bash
JEKYLL_ENV=production bundle exec jekyll build
```

### Liquid templates

In your templates, you can use the `thumbnail` filter:

```html
<img src="{{ page.image.path | thumbnail: 100 }}" />

<!-- responsive images -->

<picture>
  {% for size in site.images.sizes %}
  <source srcset="{{ page.image.path | thumbnail: size }}" media="(max-width: {{ size }}px)" />
  {% endfor %}

  <img src="{{ page.image.path | thumbnail: 750 }}" />
</picture>
```

Options for this filter are in the following order:

* `width`, the desired width for the image, if `nil`, `height` is
  required.

* `height`, if provided, the thumbnail will crop to this size.  If not,
  the image is scaled down proportionally to the width.  It becomes
  required if the width is `nil`.

* `crop` the smart cropping algorithm.  One of `none`, `centre`,
  `entropy` or `attention` (default).  See
  [Vips::Interesting](https://www.rubydoc.info/gems/ruby-vips/Vips/Interesting)
  for documentation.

* `auto_rotate`, a boolean (defaults to `true`).  If the image has
  orientation metadata, this controls if it's automatically rotated.

If you want to pass `crop` and `auto_rotate` but not `height`, just set
`height` to `nil`.

## TODO

* Use `<picture>` and the `srcset` attribute automatically for posts
  <https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture>

* Download and thumbnail images in posts and other documents.

## Contributing

Bug reports and pull requests are welcome on GitHub at
<https://0xacab.org/sutty/jekyll/jekyll-images>. This project is
intended to be a welcoming space for collaboration, and contributors are
expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-images project’s codebases, issue
trackers, chat rooms and mailing lists is expected to follow the [code
of conduct](https://sutty.nl/en/code-of-conduct/).
